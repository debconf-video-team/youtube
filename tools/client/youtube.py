from pathlib import Path

import httplib2
from googleapiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run_flow

# Currently credentials live in the base of the git repo
basedir = Path(__file__).parent / '..' / '..'


class YouTube:
    _yt = {}

    @classmethod
    def client(cls, filename='client_id.json'):
        if filename not in cls._yt:
            cls._yt[filename] = cls.build_client(filename)
        return cls._yt[filename]

    @classmethod
    def build_client(cls, filename):
        storage = Storage(str(basedir / filename))
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            credentials = cls.authorize(storage)

        http = httplib2.Http()
        try:
            http.redirect_codes = http.redirect_codes - {308}
        except AttributeError:
            # Presumably running against an older httplib, so
            # the fix isn't needed
            pass
        client = credentials.authorize(http)
        return build('youtube', 'v3', http=client)

    @classmethod
    def authorize(cls, storage):
        flow = flow_from_clientsecrets(
            str(basedir / 'client_secrets.json'),
            scope='https://www.googleapis.com/auth/youtube')
        args = type('Args', (object,), {})()
        args.noauth_local_webserver = False
        args.auth_host_port = [8000]
        args.auth_host_name = 'localhost'
        args.logging_level = 'ERROR'
        return run_flow(flow, storage, args)
