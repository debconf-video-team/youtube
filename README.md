Tools and metadata for managing the [YouTube mirror][channel] of the Debian
Meetings archive.

[channel]: https://www.youtube.com/c/DebConfVideos

The "metadata" directory contains YAML files describing playlists of videos.
These are built from metadata from the "archive-meta" repo.

"auth.py" will just authenticate to YouTube.

"upload_youtube.py" will upload to YouTube, as the name describes. It can write
a log of the youtube_ids, for merging back into metadata later.

"merge-meta.py" will merge metadata changes from the "archive-meta" repo.

"merge-uploads.py" merges a log from "upload_youtube.py" back into the metadata.

"publish-slowly.py" will publish a video a day.

[Documentation](https://debconf-video-team.pages.debian.net/youtube/)
