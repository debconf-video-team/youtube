#!/usr/bin/env python3

import argparse
import hashlib
import logging
import signal
import subprocess
import sys
import time
from pathlib import Path

import yaml

from client.errors import RateLimitedException
from client.playlist import Playlist
from client.video import Video


def load_manifest(manifest):
    return yaml.safe_load(manifest)


def transcode_video(pathname, transcode_dir):
    """Convert pathname into a format YouTube likes"""
    hash_ = hashlib.md5(pathname.encode('utf8')).hexdigest()
    converted = Path(transcode_dir, hash_).with_suffix('.mpeg')
    if not converted.exists():
        subprocess.check_call((
            'ffmpeg', '-i', pathname,
            '-vcodec', 'mpeg2video', '-acodec', 'mp2',
            '-vb', '10240k', '-ab', '384k',
            str(converted)
        ))
    return str(converted)


def post_youtube(meta, logfile, transcode, wait):
    """Post video to YouTube and log it"""
    playlist = meta.pop('playlist', None)
    youtube_id = meta.pop('youtube_id', None)
    if not youtube_id:
        pathname = meta.pop('file')
        log = {
            'file': pathname,
        }
        if transcode:
            pathname = transcode_video(pathname, transcode)
            log['transcode'] = pathname

        upload = retry_upload if wait else Video.create
        video = upload(pathname, **meta)

        log['youtube_id'] = video.id
        yaml.safe_dump(log, logfile, explicit_start=True)
        logfile.flush()

        if playlist:
            Playlist.get_by_id(playlist).insert(video)
    else:
        video = next(Video.list([youtube_id]))
        video.set_description(meta['description'])


def retry_upload(*args, **kwargs):
    """Retry Video.create if we hit API rate-limits.

    Technically, these limits could be hit on any action, but 99% of the time,
    they occur on uploads. And only uploads raise this exception.
    """
    while True:
        try:
            return Video.create(*args, **kwargs)
        except RateLimitedException:
            time.sleep(60 * 60)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('manifest', metavar='MANIFEST', type=open,
                   help='YAML upload manifest.')
    p.add_argument('log', metavar='LOG', type=argparse.FileType('a'),
                   help='File to log uploads to.')
    p.add_argument('--transcode', metavar='DIR', default=None,
                   help='Transcode video, using DIR as a temporary directory.')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity.')
    p.add_argument('--wait', '-w', action='store_true',
                   help='When rate-limited, wait an hour and retry, '
                        'automatically.')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    logging.getLogger('googleapiclient').setLevel(logging.ERROR)

    manifest = load_manifest(args.manifest)

    video_defaults = manifest.get('defaults', {})

    for video in manifest['videos']:
        # Apply defaults
        for k, v in video_defaults.items():
            if k in video and isinstance(video[k], list):
                video[k].extend(v)
            else:
                video.setdefault(k, v)

        try:
            post_youtube(video, args.log, args.transcode, args.wait)
        except RateLimitedException as e:
            sys.stderr.write(str(e) + '\n')
            sys.exit(1)
        except KeyboardInterrupt:
            sys.stderr.write('Aborted (^C)\n')
            sys.exit(128 + signal.SIGINT)


if __name__ == '__main__':
    main()
