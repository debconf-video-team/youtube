Usage example
=============

As videos are large, often the videos are hosted on a remote machine
that is different to the one you will manipulate metadata, and commit to
git on.

So, these examples will have commands being executed on 2 machines, the
"uploader machine" (e.g. apu for DebConf) and "your machine".
But you can, of course, do this all on a single machine.

The once-off setup below applies to the uploader machine.

Once-off setup
--------------

.. _credential-creation:

YouTube authentication: credential creation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can skip this if someone gives you a ``client_secrets.json``.

#. Create a `Google APIs`_ project.
   https://console.developers.google.com/apis/

.. _Google APIs: https://console.developers.google.com/apis/

#. Enable the YouTube Data v3 API in the "Library".

#. Create a "Desktop" OAuth 2.0 client ID in the "Credentials".

#. Download the Client ID JSON, and save it as ``client_secrets.json``.

.. _authorization:

YouTube authentication: authorization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Get a ``client_secrets.json`` from another team member, or generate
   your own, following the :ref:`YouTube credential creation
   steps<credential-creation>`.

#. Run ``auth.py`` (in the virtualenv, below). It will run a webserver
   on ``localhost:8000``. Port-forward this to your local machine, if
   you are running it on a remote server.
   It will also attempt to open a URL in a web browser. If that isn't
   possible, the URL is printed out for you to open by hand.

#. Visit that URL, and sign in with a Google account that has admin
   access to the YouTube channel. Select the channel, and authorize the
   app to access it.

#. You can now delete ``client_secrets.json``, if you want.
   The uploader only needs ``client_id.json``.

Virtualenv creation
~~~~~~~~~~~~~~~~~~~

This uploader requires a few Python libraries (see
``requirements.txt``).
You can either install them from Debian, or create a virtualenv to
contain them.

#. Create a new Python virtualenv::

     python3 -m venv ve
     ve/bin/python -m ensurepip -U
     ve/bin/python -m pip install wheel

#. Install the required packages in the virtualenv::

     ve/bin/python -m pip install -r requirements.txt

#. Activate the virtualenv before using these tools, every time::

     . ve/bin/activate

Import videos from a conference
-------------------------------

#. Upload the videos to the meetings-archive.

#. Describe them in archive-meta.

#. Run ``merge-meta``::

     tools/merge-meta.py ../archive-meta/metadata/

#. A new metadata file should be created with your new playlist.
   Check the contents, edit archive-meta if necessary, and repeat until
   satisfied with the data.

#. Check and update the ``defaults``, applied to every upload.

#. Optional: Create a playlist in YouTube, and record this in the
   ``defaults``.

#. Copy this YouTube metadata file to the uploader machine with the
   meetings-archive available locally.

#. Run the upload script, on the uploader machine, in the base directory
   for the conference videos, recording uploaded video IDs to a log.

   The first time you do this, you'll have to :ref:`authorize
   it<authorization>`.

   ::

     cd /srv/video/video.debian.net/2019/example/
     ~/youtube/tools/upload_youtube.py --wait \
         ~/youtube/metadata/2019/example.yml ~/upload.log

#. Copy ``upload.log`` back to your machine.

#. Merge the video IDs from it into the metadata file::

     tools/merge-uploads.py metadata/2019/example.yml upload.log

#. The ``--wait`` will cause the uploader to wait and retry, until the
   entire playlist is uploaded, even if YouTube rate-limits are reached.

   If you aren't using ``--wait``, or have another failure, and need to
   continue a partial upload:

   #. Ensure the uploaded video IDs have been merged into the metadata
      (from the above steps).

   #. Update the metadata on the uploader machine.

   #. Run the upload script again. It will upload videos without
      ``video_id`` attributes.
