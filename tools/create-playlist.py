#!/usr/bin/env python3

import argparse
import logging

from client.playlist import Playlist


def main():
    p = argparse.ArgumentParser()
    p.add_argument('name',
                   help='Playlist Name')
    p.add_argument('--unlisted', action='store_const', dest='privacy',
                   default='public', const='unlisted',
                   help='Keep the Playlist unlisted')
    p.add_argument('--private', action='store_const', dest='privacy',
                   default='public', const='private',
                   help='Keep the Playlist private')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    logging.getLogger('googleapiclient').setLevel(logging.ERROR)

    p = Playlist.create(title=args.name, privacy=args.privacy)
    print(p.id)


if __name__ == '__main__':
    main()
