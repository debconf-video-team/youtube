# -*- coding: utf-8 -*-
import json
import logging
import time
import mimetypes

from googleapiclient.http import MediaFileUpload
from googleapiclient.errors import HttpError, ResumableUploadError

from client.errors import RateLimitedException
from client.utils import iter_pages
from client.youtube import YouTube

log = logging.getLogger('client.video')


class Video:
    def __init__(self, data):
        self._yt = YouTube.client()
        self.data = data

    @property
    def id(self):
        return self.data['id']

    @property
    def url(self):
        return 'https://youtu.be/{}'.format(self.id)

    @property
    def privacyStatus(self):
        return self.data['status']['privacyStatus']

    @property
    def uploadStatus(self):
        return self.data['status']['uploadStatus']

    @classmethod
    def create(cls, path, title, description='', category=27,
               default_language='en-us', license='creativeCommon',
               privacy='public', tags=()):
        youtube = YouTube.client()
        body = {
            'snippet': {
                'title': title,
                'categoryId': category,
                'description': description,
                'defaultLanguage': default_language,
                'tags': tags,
            },
            'status': {
                'embeddable': True,
                'privacyStatus': privacy,
                'license': license,
            }
        }
        mimetype, encoding = mimetypes.guess_type(path)

        log.info('Uploading: %s', title)

        # size of each upload request
        chunksize = 128 * 1024 * 1024
        request = youtube.videos().insert(
            part='snippet,status', body=body,
            media_body=MediaFileUpload(
                path, mimetype=mimetype, chunksize=chunksize, resumable=True
            ))

        while True:
            try:
                status, response = request.next_chunk()
            except ResumableUploadError as e:
                error_body = json.loads(e.content.decode('utf-8'))
                for error in error_body['error']['errors']:
                    if error['reason'] == 'quotaExceeded':
                        log.error('Hit YouTube rate-limit. Try again, later.')
                        raise RateLimitedException(error['message'])
                log.error('Upload failed: %s', error_body['error']['message'])
                raise
            except HttpError as e:
                if 500 <= e.resp.status < 600:
                    log.info('Server error %s, retrying...', e.resp.status)
                    time.sleep(5)
                    continue
                raise

            if response is None:
                log.info('Progress: %i%%', 100 * status.progress())
                continue

            return Video(response)

    def set_description(self, description):
        body = {
            'id': self.id,
            'snippet': self.data['snippet'].copy(),
        }
        if description == body['snippet']['description']:
            log.debug('Nothing to do')
            return
        body['snippet']['description'] = description
        youtube = YouTube.client()
        youtube.videos().update(part='snippet', body=body).execute()
        log.info('Changed description on %s', self.data['snippet']['title'])

    def publish(self, privacyStatus='public'):
        body = {
            'id': self.id,
            'status': {
                'privacyStatus': privacyStatus,
            },
        }
        youtube = YouTube.client()
        youtube.videos().update(part='status', body=body).execute()
        log.info('Changed privacyStatus on %s to %s',
                 self.data['snippet']['title'], privacyStatus)

    @classmethod
    def list(cls, ids):
        youtube = YouTube.client()
        if not isinstance(ids, list):
            raise ValueError('ids must be a list')

        for item in iter_pages(
                youtube.videos().list,
                id=','.join(ids),
                part='snippet,status,contentDetails'):
            yield Video(data=item)

    def delete(self):
        self._yt.videos().delete(id=self.id).execute()
