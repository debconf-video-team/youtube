#!/usr/bin/env python3

import argparse
import logging
import sys
import time
import yaml

from client.video import Video


def load_manifest(manifest):
    return yaml.safe_load(manifest)


def publish(youtube_id):
    """Publish a video

    Return True if the video was previously not public
    """
    video = next(Video.list([youtube_id]))
    if video.privacyStatus == 'public':
        return False
    if video.uploadStatus != 'processed':
        return False
    video.publish()
    return True


def main():
    p = argparse.ArgumentParser()
    p.add_argument('manifest', metavar='MANIFEST', type=open,
                   help='YAML upload manifest')
    p.add_argument('--batch', metavar='NUMBER', type=int, default=1,
                   help='Publish NUMBER of videos in every patch.')
    p.add_argument('--wait', metavar='DAYS', type=int, default=0,
                   help='Wait DAYS days between each batch of publishes.')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    logging.getLogger('googleapiclient').setLevel(logging.ERROR)

    manifest = load_manifest(args.manifest)
    youtube_ids = (video['youtube_id']
                 for video in manifest['videos']
                 if 'youtube_id' in video)

    while True:
        for i in range(args.batch):
            for youtube_id in youtube_ids:
                if publish(youtube_id):
                    break
            else:
                # Everything is published
                sys.exit(0)

        if args.wait == 0:
            break
        time.sleep(args.wait * 24 * 60 * 60)


if __name__ == '__main__':
    main()
